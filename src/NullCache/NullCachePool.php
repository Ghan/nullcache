<?php
namespace Lurch\NullCache;

use Psr\Cache\CacheItemPoolInterface;
use Psr\Cache\CacheItemInterface;

class NullCachePool implements CacheItemPoolInterface {
  
  public function getItem($key): NullCacheItem { 
    return new NullCacheItem($key);
  }
  
  public function getItems(array $keys = array()) {
    foreach ($keys as $key)
      yield new NullCacheItem($key);
  }
  
  public function hasItem($key){
    return false;
  }
  
  public function clear() {}
  public function deleteItem($key) {}
  public function deleteItems(array $keys) {}
  public function save(CacheItemInterface $item) {}
  public function saveDeferred(CacheItemInterface $item) {}
  public function commit() {}
}

class NullCacheItem implements CacheItemInterface {
  private  $key = null;    
  public function __construct($key) {
    $this->key = $key;
  }
  
  public function getKey() {
    return $this->key;
  }
  
  public function get() {
    return null;
  }
  
  public function isHit() {
    return false;
  }
  
  public function set($value) {}
  public function expiresAt($expiration) {}
  public function expiresAfter($time) {}
}